module.exports = {
    template: document.querySelector('.add_app').innerHTML,
    data(){
        return {
            name:"",
            url:""
        }
    },
    methods:{
        saveApplication(){
            this.$store.dispatch('saveApplication',{
                name: this.name
            })
        }
    }
}