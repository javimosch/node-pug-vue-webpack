const webpack = require('webpack');
const path = require('path')
const VueLoaderPlugin = require('vue-loader/lib/plugin')

async function compile() {
    return new Promise((resolve, reject) => {
        const compiler = webpack({
            entry: path.join(process.cwd(), 'src/client.js'),
            output: {
                path: path.join(process.cwd(), 'dist/bundle.js'),
                filename: 'bundle.js'
            },
            module: {
                rules: [
                  // ... other rules
                  {
                    test: /\.vue$/,
                    loader: 'vue-loader'
                  },
                  {
                    test: /\.pug$/,
                    oneOf: [
                      // this applies to `<template lang="pug">` in Vue components
                      {
                        resourceQuery: /^\?vue/,
                        use: ['pug-plain-loader']
                      },
                      // this applies to pug imports inside JavaScript
                      {
                        use: ['raw-loader', 'pug-plain-loader']
                      }
                    ]
                  },
                  {
                    test: /\.scss$/,
                    use: [
                      'vue-style-loader',
                      'css-loader',
                      {
                        loader: 'sass-loader',
                        options: {
                          // you can also read from a file, e.g. `variables.scss`
                          // use `prependData` here if sass-loader version = 8, or
                          // `data` if sass-loader version < 8
                          additionalData: `$color: red;`
                        }
                      }
                    ]
                  }
                ]
              },
              plugins: [
                // make sure to include the plugin!
                new VueLoaderPlugin()
              ]
        });

        compiler.run((err, stats) => { // Stats Object

            console.log(err, stats)
            if (err) {
                reject(err)
            } else {
                resolve(stats)
            }
        });
    })
}
module.exports = {
    compile
}