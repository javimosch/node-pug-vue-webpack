const path = require('path')
const express = require('express')
const morgan = require('morgan')
const bunyan = require('bunyan');
const appName = process.env.APP_NAME || 'apping'
const log = bunyan.createLogger({ name: appName });
const app = express()
const funql = require('funql-api')
const StormDB = require("stormdb");
const engine = new StormDB.localFileEngine("./db.stormdb",{
    async: true
  });
const db = new StormDB(engine);
const pugVue = require('./server/modules/pug-vue')
const pugVueCompilerOptions = {
    src: path.join(process.cwd(),'src','components')
}

db.default({ users: [], apps:[] });

funql.middleware(app, {
    attachToExpress:true,
    api: {
        async saveApplication(app) {
            if(!db.get('apps').value()){
                db.get('apps').set([])
            }
            console.log("FOUND",db.get('apps').filter(_app=>_app.name==app.name))
            if(db.get('apps').filter(_app=>_app.name==app.name).value().length>0){
                return {
                    err: "NAME_DUPLICATED"
                }
            }
            await db.get('apps').push(app).save()
            return true
        }
    }
})

app.set('views', path.join(process.cwd(), 'src/views'))
app.set('view engine', 'pug')
app.use(morgan('tiny'))
app.use('/js', express.static(path.join(process.cwd(),'src','js')))
app.use('/css', express.static(path.join(process.cwd(),'src','css')))
app.get('/bundle.js',(req,res)=>{
    (async()=>{
        await require('./server/webpack').compile()
        res.sendFile(path.join(process.cwd(),'dist/bundle.js'))
    })()
})
app.get('/', function (req, res) {
    res.render('home', {
        ...pugVue.injectCompiler(pugVueCompilerOptions),
        title: appName, data: {
            users: [],
            apps: db.get('apps').value()
        }
    })
})
app.get('/users/save',(req,res)=>{

})
app.listen(process.env.PORT || 3000, () => log.info(`Listen`, process.env.PORT || 3000))